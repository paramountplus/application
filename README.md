![picture](https://i.imgur.com/ivpHTyt.png)

Unofficial Paramount+ desktop application.

Enjoy all your Paramount+ movies and TV shows all on the open source desktop application

 &nbsp;&nbsp;&nbsp;&nbsp;

  You can install Youtube from the AUR for Arch/Manjaro distros.
 [Click Here](https://aur.archlinux.org/packages/paramountplus/)

 ### Download For All platforms (Linux, Mac OS and Windows)
  
  [Click to get the latest release](https://gitlab.com/paramountplus/application/-/releases)

 ### Author
  * Corey Bruce
